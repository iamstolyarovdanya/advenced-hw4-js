const container = document.createElement(`div`)
container.classList.add(`container`);
document.querySelector(`body`).append(container)
fetch(`https://ajax.test-danit.com/api/swapi/films`)
    .then(res => res.json())
    .then(data =>
        data.forEach((elem) => {
            let div = document.createElement(`div`);
            div.style.order = elem.episodeId;
            let h1 = document.createElement(`h1`);
            let p = document.createElement(`p`);
            let h2 = document.createElement(`h2`)
            h1.textContent = `Episode ${elem.episodeId}. ${elem.name}`
            p.textContent = `${elem.openingCrawl}`
            h2.textContent = `Actors :`;
            container.append(div);
            div.append(h1, p, h2);
            elem.characters.forEach((e) => {
                fetch(e)
                    .then(res => res.json())
                    .then(data => {
                        let ul = document.createElement(`ul`);
                        let li = document.createElement(`li`);
                        li.textContent = `${data.name}`;
                        div.append(ul)
                        ul.append(li)
                    })
            })
        }))
    .catch((err) => {
        console.log(err)
    })